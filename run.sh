#!/bin/sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
rm log.txt
make clean
make
echo "Resolution" >> log.txt
echo "240x160" >> log.txt
echo "320x240" >> log.txt
echo "480x360" >> log.txt
echo "640x480" >> log.txt
echo "1080x720" >> log.txt
echo "1920x1080" >> log.txt

for trial in 1 2 3 4 5
do
echo "Trial Number"  $trial >> log.txt
./saliency_app donald_duck_240x160.bmp >> log.txt
./saliency_app donald_duck_320x240.bmp >> log.txt
./saliency_app donald_duck_480x360.bmp >> log.txt
./saliency_app donald_duck_640x480.bmp >> log.txt
./saliency_app donald_duck_1080x720.bmp >> log.txt
./saliency_app donald_duck_1920x1080.bmp >> log.txt
done
