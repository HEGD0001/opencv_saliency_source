#LIBS  = -lm -lgcc -lopencv_highgui -lopencv_imgproc -lopencv_objdetect -lopencv_core
LIBS  = -lm -lgcc `pkg-config opencv --cflags --libs`
CFLAGS = -Wall -O3
CC = g++
OBJ = opencv_saliency.o	

saliency_app: $(OBJ)
	$(CC) $(LIBS) -o $@ $^ $(CFLAGS) $(LIBS)
%.o : %.cpp
	$(CC) -c $(CFLAGS) $< -o $@


clean:
	rm *.o
	rm saliency_app
	rm *_map.bmp
